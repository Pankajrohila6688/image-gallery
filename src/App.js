import React, { useState } from 'react';
import './App.css';
import SearchBar from './components/SearchBar';
import ImageList from './components/ImageList';
import unsplash from './api/unsplash';

function App() {

  const [images, setImages] = useState([]);

  const onSearchSubmit = async (term) => {
    const response = await unsplash.get('/search/photos', {
        params: {query: term}
    });
    setImages(response.data.results)
    console.log(response.data.results)
  }

  return (
    <div className="app">
      <div className="ui container" style={{ marginTop: '10px' }}>
        <SearchBar onSubmit={onSearchSubmit} />
        <ImageList images={images} />
      </div>
    </div>
  );
}

export default App;
