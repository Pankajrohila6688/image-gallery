import React, { useState } from 'react';
// import { InputGroup, FormControl } from 'react-bootstrap';

const SearchBar = ({ onSubmit }) => {

    const [term, setTerm] = useState('');

    const onFormSubmit = e => {
        e.preventDefault();

        onSubmit(term);
    };

    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <div style={{ display: 'flex', padding: '35px' }}>
                    <input
                        style={{ width: '100%', padding: '15px' }} 
                        type="text" 
                        placeholder="Search for Photo"
                        value={term}
                        onChange={e => setTerm(e.target.value)} 
                    />
                    <button style={{ marginLeft: '10px', padding: '10px', backgroundColor: 'black', border: 'none', color: 'white', borderRadius: '5px', cursor: 'pointer' }} type="submit">Search</button>
                </div>
                
            </form>
        </div>
    )
}

export default SearchBar;