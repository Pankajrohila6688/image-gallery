import React, { useEffect, useState } from 'react';

const ImageCard = ({ image }) => {

    const [spans, setSpans] = useState(0);
    const imageRef = React.createRef();
    
    const setSpan = () => {
        const height = imageRef.current.clientHeight;

        const spans = Math.ceil(height / 10);

        setSpans(spans);
    }
    
    useEffect(() => {
        imageRef.current.addEventListener('load', setSpan);
    }, [imageRef, setSpan]);


    const { description, urls } = image;

    return (
        <div>
            <div style={{ marginRight: '20px', marginLeft: '20px', }}>
                <img style={{ height: '200px', width: '300px', margin: '10px', borderRadius: '5px' }} ref={imageRef} alt={description} src={urls.regular} />  
            </div>
        </div>
    );
}

export default ImageCard;